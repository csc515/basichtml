package edu.missouristate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasichtmlApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasichtmlApplication.class, args);
	}

}
